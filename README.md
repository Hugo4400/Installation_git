# Comment installer et utiliser git

Ce tutoriel est basé sur l'utilisation de gitlab mais peux etre adapté pour d'autres sites.
Ce tutoriel n'est pas complet et ne devrais pas servir comme remplacement a la documentation git.

* <a href=#step1 > Etape 1: Telechargement et installation </a>
* <a href=#step2 > Etape 2: Installation de cmder (optionel) </a>
* <a href=#step3 > Etape 3: Installation de XCode (mac uniquement) </a>
* <a href=#step4 > Etape 4: Verification et ajout de global name </a>
* <a href=#step5 > Etape 5: Les comandes git </a>
  * <a href=#init > init </a>
  * <a href=#clone > clone </a>
  * <a href=#add > add </a>
  * <a href=#commit > commit </a>
  * <a href=#push > push </a>
  * <a href=#pull > pull </a>
* <a href=#step6 > Etape 6: Creation ou telechargement d'un repositoire </a>
* <a href=#step7 > Etape 7: Les clées SSH (optionel) </a>
* <a href=#step8 > Etape 8: Integration dans PHPStorm (optionel) </a>

## <div id="step1"></div> Etape 1: Telechargement et installation
-----

Telechagez l'installer [**ici**](https://git-scm.com/)

#### Si sur windows continuez a lire, si sur mac avancez a <a href=#step3 > l'etape 3. </a>
Une fois le téléchargement fini lancez l'installation.

<img src="http://i.imgur.com/gEUyyzf.png">

Acceptez la licence etc..

<img src="http://i.imgur.com/jIQYuza.png">

Premiere section: Choisissez si vous voulez un incone sur le bureau ou pas

Deuxieme section: Vous permet d'ouvrir une ligne de commande git a l'endroit ou vous ettes avec un clique droit (cochez ceci si sa ne vous derange pas d'avoir encore plus d'options dans le menu clique droit, c'est tres pratique par contre car ca evite de faire des ``` cd dossier/autre_dossier/encore_un_autre/nomfichier``` tout le temps.

Gardez le reste coché comme dans l'image.

<img src="http://i.imgur.com/gUl68IF.png">

Cochez la derniere option pour pouvoir utiliser des commandes bash dans la cmd windows (presque obligatoire)!

Si vous cochez la premiere option il est obligatoire d'utiliser l'exe "git bash" pour toutes choses git.

La deuxieme option permet de faire des commandes git dans la cmd windows mais la création de clées SSH (par exemple) devra etre fait avec "git bash".

<img src="http://i.imgur.com/rseXUyh.png" >

Cochez ca pour éviter des conflits de sauts de lignes (ils ne sont pas traité de la meme facon entre widows et unix)

<img src="http://i.imgur.com/9RS4Jmi.png" >

N'utilisez pas MinTTY.

<img src="http://i.imgur.com/lCTNAcx.png" >

Finalement activez ca et cliquez sur "next" pour installer.

## <div id="step2"></div> Etape 2: Installation de cmder (optionel)
-----

Si vous n'aimez pas cmd, git bash ou powershell (a ce stade la vous avec les trois sur votre machine, peut etre pas powershell en fonction de votre version de Windows.) vous pouvez telecharger cmder [**en cliquant ici.**](http://cmder.net/) Prenez la version full pour pouvoir utiliser git et bash dans cmder.

L'instalation est tres simple, il suffit de telecharger le .zip et d'extraire les fichiers ou vous voulez (par exemple /programfiles) puis de faire un raccourci vers le fichier .exe et de mettre ce raccourci ou vous voulez!

cmder est une terminal beaucoup plus belle et qui a un peux plus de fonctionalité mais par contre son installation ne vous ajout pas plus de fonctionalitées git.

## <div id="step3"></div> Etape 3: Installation de XCode (mac uniquement)
-----

Téléchargez et installez git.

Dans la terminal mac tapez ```git```.

Normalement une fenetre devrait s'ouvrir pour vous demander de telecharger XCode, faites le.

Et voila, vous pouvez maintenant utiliser git!

## <div id="step4"></div> Etape 4: Verification et ajout de global name
-----

Maintenant ca devient plus interessant.

Dans la terminal de votre choix (cmd, git bash, cmder, powershell, git shell etc..) tapez ``` git ```

Si vous avez bien fait l'installation vous devrez voir ceci:

<img src="https://i.imgur.com/ZgIuTGs.png">

Comme vous pouvez le voir ceci est aussi une page de reference au cas ou vous avez besoin de voir les commandes possibles. ``` git help <nom de votre commande>``` permet d'avoir plus de detail.

Maintenant il vous faut un ``` global.name ``` et ``` global.email ``` cela veux dire que a chaque fois que vous envoyez une modification les autres pouront voir qui l'a fait.

``` bash
	git config --global user.name "Prenom Nom"
	git config --global user.email "adresse@mail.com"
```

Tapez ca dans la terminal et c'est fait! (pour de bon, plus besoin de le refaire)

## <div id="step5"></div> Etape 5: Les comandes git
-----

Voici la partie la plus importante donc faites bien attention!

### <div id="init"></div> init

``` bash
	git init
```
Créer un repositoire git vide.

ou

Reinitialise un repertoire git (cela veux dire que pou faire un commit il va falloir reselectioner les fichiers a ajouter et il faut remetre l'adresse de la branche sur gitlab).

Git init sera mieux expliqué avec un exemple dans <a href=#step6> **l'etape 6** </a>

### <div id="clone"></div> clone

``` bash
	git clone https://gitlab.com/<Username>/<Reponame>.git
```
Fait une copie du repositoire dans votre ordinateur.

Si quelqun a crée un projet dans gitlab (ou github, gitbucket, etc..) et vous voulez travailler dessu il faut faire ceci.

Tres souvent il est possible de faire un ``` git clone ``` sur quelque chose et apres de ne pas pouvoir faire de commit. Cela veux dire que vous avez le droit de telecharger mais vous n'avez pas les permitions en tant que developpeur, c'est le cas pour beaucoup de projets publiques sur internet et donc ne paniquez pas si vous avez des erreurs!

Noubliez pas que le clone s'éffectue la ou votre terminal est situé (donc si vous ettes dans /system32 vous allez galerer a trouver le repositoire) la commande ``` cd ``` vous permet de bouger de fichier et donc de vous mettre la ou vous voulez.

Dans le cas de gitlab, les repositoires peuvent etre disponibles que pour les utilisateurs avec un compte et donc la terminal vous demandera votre nom d'utilisateur et mot de passe. (le mot de passe ne s'affiche pas du tout sur l'ecran de la terminal donc ne pensez pas qu'il y a une erreur si vous tapez et rien n'apparait!)

### <div id="add"></div> add

``` bash
	git add nomfichier
	git add nomdossier
	git add .
```

Ajoute un fichier ou dossier a la liste de choses a déposer sur le serveur.

De simplement naviguer dans le dossier ou se trouve votre code et de faire ``` git add . ``` ajoutera tout ce qui ce trouve dans ce dossier, c'est pratique pour eviter d'oublier des choses.

Par contre si le fichier en question est dans le meme dossier que plein de choses innutiles il faut utiliser ``` git add nomfichier ``` (ou ``` git add nomdossier ``` si vouz voulez ajouter un dossier entier).

Il est important de remarquer que ``` git add ``` ne crée aucun fichier, il est juste utilisé pour séléctioner ce que vouz voulez envoyer.

Vous avez besoin de faire add qu'une fois par fichier/dossier dans un projet sauf si vous avez reinitalisé avec ``` git init ```.

### <div id="commit"></div> commit

``` bash
	git commit -m "message obligatoire"
```

La fonction la plus utilisé!

```git commit``` créer un point de sauvegarde auquel vous pouvez retourner a tout moment. Ce point de sauvegarde contient aussi votre nom et email que vous avez ajouté avec ``` git config ``` dans l'etape 4. Votre message est utile pour vous et les autres a savoir ce que vous avez fait (et pourquoi) donc essayez d'etre descriptif!

Un commit est obligée d'avoir un message (meme si c'est du nimporte quoi).

Un commit est obligée d'avoir un changement aux fichiers.

Si vouz voulez ajouter un fichier sur le repositoire internet (qui n'y est pas déja) il faut faire ``` git add ``` puis ``` git commit ``` car commit sauvegarde que les fichiers choisis par add.

Faites des commit souvent! C'est mieux de retourner en arriere d'une heure que d'une semaine!

utilisez ``` git commit -m "message obligatoire" ``` pour un commit plus rapide.

Si vous oubliez le ``` -m "message obligatoire" ``` la terminal ouvrira Vim pour taper votre message! Ne paniquez pas, tapez votre message (il apparaitera en haut de la fenetre) et puis appuyez sur *** echap *** puis tapez *** :wq *** et voila!

### <div id="push"></div> push

``` bash
	git push -u origin master
```

Il est inutile de faire des motifications avec git si vous ne le les mettez pas sur le serveur...

Push mets tout vos commit sur le repositoire sur internet.

Faites un push que quand vous avez fini pour la seance (pour eviter d'envoyer du code moitié fait a vos collaborateurs).

Comme un push envoi tous vos commit, les autres peuvent remonter sur les points de sauvegardes que vous avez fait.

###### Dans le cas ou vous n'avez pas fait un ```git clone ``` car vous voulez envoyer un nouveau projet qui existe sur votre machine mais pas sur gitlab:

* il faut creer le projet sur gitlab en premier.

* puis il faut definir ou envoyer les donées avec

``` bash
	git remote add origin https://gitlab.com/<username>/<reponame>.git
```

* apres vous pouvez envoyer avec push.

Dans certains cas il faut utiliser

``` bash
	git push origin https://gitlab.com/<username>/<reponame>.git
```

### <div id="pull"></div> pull

``` bash
	git pull https://gitlab.com/<username>/<reponame>.git
```

L'inverse de push!

pull prend les commits fait pas les autres et mets a jours vos fichiers.

## <div id="step6"></div> Etape 6: Creation ou telechargement d'un repositoire

-----
##### Creation d'un nouveau reposotoire qui n'existe pas sur votre machine:

* Créez un projet dans gitlab

* Pour creer un projet dans un groupe utilisez "create project" sur la page du groupe ou dans la section "namespace" de la page de creation de projet selectionez le nom du groupe (risque de ne pas apparaitre la premiere fois que vous le faites).

* Utilisez ce code dans votre terminal

``` bash
	git clone https://gitlab.com/<username>/<reponame>.git
	cd <reponame>
	touch README.md
	git add README.md
	git commit -m "add README"
	git push -u origin master
```

La creation du readme.md peux etre fait dans gitlab directement et donc n'est pas nessesaire, dans ce cas la ca sert comme exemple pour vous montrer un add,commit, et push.

##### Ajout d'un répositoire qui existe sur votre machine mais pas sur gitlab:

* Créez le projet comme avant

* Utilisez ce code dans votre terminal

``` bash
	cd dossier ou se trouve vos fichiers
	git init
	git remote add origin https://gitlab.com/<username>/<reponame>.git
	git add .
	git commit
	git push -u origin master
```

## <div id="step7"></div> Etape 7: Les clées SSH (optionel)
-----

Vous avez peu etre remarqué que a chaque fois que vous voulez faire un push, pull ou clone vous avez besoin d'entrer votre nom d'utilisateur et mot de passe.

Les clées SSH vous simplifient la vie!

Dans un projet gitlab vous avez le choix entre l'adresse HTTPS ou SSH, dans tous les exemples de ce tutoriel nous avions utilisé HTTPS.

Une clé SSH est en fait deux choses, une clé publique et privée. La clé privée ne doit pas etre mis sur internet! Elle est unique a votre!

La clé publique est ce qui nous interesse, c'est ca qu'on donnera a gitlab pour pouvoir avoir une connection 100% sécurisée et donc pas besoin d'entrer de mot de passe a chaque push!

Il est possible que votre machine a déja une clé SSH, pour vérifier tapéz ca dans une terminal qui peux interpreter du bash:

``` bash
	cat ~/.ssh/id_rsa.pub
```

Si vous voyez un gros pavé de lettres et de chiffres, c'est bon!

Mais il est fort probable que non donc il faut entrer ca dans la terminal

``` bash
	ssh-keygen -t rsa -C "<votre adresse @ mail>"
```

et puis

``` bash
	cat ~/.ssh/id_rsa.pub
```

pour l'afficher

si votre terminal ne peux pas faire de copier coller utiliser

``` bash
	clip < ~/.ssh/id_rsa.pub
```

pour copier la clé

ou sur mac:

``` bash
	pbcopy < ~/.ssh/id_rsa.pub
```

Apres dans le menu paramettres de votre compte gitlab vous pouvez ajouter des clées SSH et donc vous collez le code dans cette page et vous le donnez un nom (car si vous avez plusieurs machines il faut le faire pour chaque).

## <div id="step1"></div> Etape 8: Integration dans PHPStorm (optionel)
-----

Ceci est pas compliqué et vous permettra de plus facilement faire des commits et push sans entrer une seule ligne de commande.

En premier il faut [** créer un compte étudiant **](https://www.jetbrains.com/student/) JetBrains pour pouvoir avoir une licence gratuite.

Apres vous pouvez téléchager PHPStorm et tout les autres produits JetBrains.

Si vous etes habituées a Sublimetext, Brackets, JGrasp, etc.. vous allez avoir besoin de comprendre comment fonctionne le systeme de projets, (c'est tres tres simple) mais ce tutoriel n'est pas a propos de l'utilisation d'un IDE.

** (a faire qu'une seule fois) ** Le premiere étape pour configurer git avec PHPStorm est d'aller dans le menu peremettres.

<img src="https://i.imgur.com/wZLYTkk.png?1" width=200>

et puis de trouver la section "Version Control" et de cliquer sur "Git"

<img src="http://i.imgur.com/judLe9g.png?1">

trouvez le chemin absolue de votre fichier "git.exe", norlmalement il doit etre pareil que celui dans l'image.

Le bouton "test" vous permet de voir si vous avez le bon chemin.

**(Le reste est a faire pour chaque projet)** Il faut que vous aviez crée votre projet dans gitlab avant que vous pouvez faire des push (sauf si vous avez cloné un répo).

Apres cela sous l'onglet "VCS" dans la barre du heaut cliquez sur "enable version control intergration"

Puis dans le drop-down choisisez "git".

Normalement PHPStorm vous donne une petite notification disant que le fichier .git a été crée.

Dans le navigateur de fichiers de projet (la partie gauche de l'écran) faites un click droit sur chaque fichier que vous voulez ajouter a votre repo gitlab et dans le menu clique droit allez dans "git > add"

<img src="https://i.imgur.com/IEBlMnV.png">

Une fois que vous avez ajoutées les fichiers que vous voulez mettre dans votre commit cliquez sur l'icone avec la fleche verte en heaut a droite de l'écran (visible dans la capture d'écran).

La fenetre commit vous nessesite d'avoir au moins fait un changement aux fichiers selectionées avec "add".

Il faut un message obligatoirement mais il n'est pas obligatiore de mettre un auteur (par contre c'est reccomandé). Le nom s'auteur est ecrit ``` Prenom Nom <adresse@mail.com> ```

Apres avoir ajouté vtre message cliquez sur commit ou "commit and push" pour faire les deux.

L'ors d'un commit si il y a des erreurs ou des warnings PHPStorm bous signalera mais vous pouvez les igniorer et continuer.

Dans la fenetre push vous devez cliquer sur "define remote"

<img src="https://i.imgur.com/aEgD8J2.png">

la vous lui donnez un nom (origin par default) et vous collez le lien HTTPS ou SSH de votre repo gitlab.

Cliquez sur push est c'est envoyé! une fois configuré pour le projet l'envoie des push ce fait en un click.

Si le lien est HTTPS et le repo n'est pas public PHPStorm demandera votre nom de compte et mot de passe.

Si le lien est SSH et vous avez configuré les clées il vous suffit de cliquer sur accepter la premiere fois et apres, tout vos futurs push seront en un click.

<meta author="Hugo Moracchini">